export interface DATA
{
	session: 
	{
		secret: string
	},
	google:
	{
		clientID: string,
		clientSecret: string
	}
}

export interface SQL
{
	host: string,
	user: string,
	password: string,
	database: string,
	acc_database: string
}

export interface Student
{
	id: number,
	googleID: string|null,
	username: string,
	firstName: string|null,
	lastName: string|null,
	email: string|null,
	accountLevel: number,
	gradYear: number
}

export interface Teacher
{
	id: number,
	googleID: string|null,
	username: string,
	firstName: string|null,
	lastName: string|null,
	email: string|null,
	accountLevel: number,
	depts: string,
	room: string|null,
	maxStudents: number
}

export interface ReqUser
{
	id: number,
	googleID: string|null,
	username: string,
	firstName: string|null,
	lastName: string|null,
	email: string|null,
	accountLevel: number,
	realIdentity: number,
	gradYear: number|undefined,
	depts: string|undefined,
	room: string|null|undefined,
	maxStudents: number|undefined
}

export interface Settings
{
	school: string,
	name: string,
	day: string,
	schoolEndHrs: string,
	schoolEndMins: string,
	purposes: Array<string>,
	depts: string,
	weekday: string
}

export interface Params
{
	enabled: boolean,
	week: string
}