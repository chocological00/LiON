import fs from 'fs';
import readline from 'readline';
import mysql from 'mysql2';
import JSON5 from 'json5';

import { SQL } from './types';

const main = async () =>
{
	const SQL: SQL = await JSON5.parse(fs.readFileSync('data/SQL.json', 'utf8')) as SQL;
	const conn = mysql.createConnection(
		{
			host: SQL.host,
			user: SQL.user,
			password: SQL.password,
			database: SQL.database
		});
	conn.connect();
	const rl = readline.createInterface(
		{
			input: fs.createReadStream('data/example-defaults.sql'),
			terminal: false
		});
	rl.on('line', (chunk) =>
	{
		conn.query(chunk, (err) =>
		{
			if (err) console.log(err);
		});
	});
	rl.on('close', () =>
	{
		console.log('finished');
		conn.end();
	});
};

void main();