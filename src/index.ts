// NOTE: for the routes, field and user level errors are handled by 'return res.redirect()' to reduce tabs
//       DB related errors are still handled by if else

// Initializing Dependencies

// Filesystem access
import fs from 'fs';

// JSON5 parser
import JSON5 from 'json5';

// express web framework
import express from 'express';
const app = express();
app.disable('x-powered-by'); // hide server software information from response header - better for security

// express middlewares

// POST parser
import bodyParser from 'body-parser';
app.use(bodyParser.urlencoded({extended: true}));

// Pug template engine
app.locals.pretty = true; // eslint-disable-line @typescript-eslint/no-unsafe-member-access
app.set('views', './views');
app.set('view engine', 'pug');

// MariaDB connector
import mysql from 'mysql2/promise';

// MariaDB Session Store
import * as sessionTyped from 'express-session';
import session from 'express-session';
import MySQLSessionStore from 'express-mysql-session';
const MySQLStore = MySQLSessionStore(session as unknown as typeof sessionTyped); // monkey patch for incorrect typings

// flash session store - handles error msg
import flash from 'connect-flash';
app.use(flash());

// Passportjs Authentication Library
import passport from 'passport';
import passportGoogle from 'passport-google-oauth20';
const GoogleStrategy = passportGoogle.Strategy;

// CronJob
import cron from 'cron';
const CronJob = cron.CronJob;

// util file
import * as util from './util';

// Typescript type configs
import { DATA, SQL, Student, Teacher, Settings } from './types';

// routings
import Routes from './routes';

// main function
async function main()
{
	// log the date started
	// eslint-disable-next-line no-console
	console.log();
	// eslint-disable-next-line no-console
	console.log(new Date().toDateString() + ' ' + new Date().toTimeString());


	// Read file data
	process.stdout.write('Reading Credentials... ');
	const SQL: SQL = await JSON5.parse(fs.readFileSync('data/SQL.json', 'utf8')) as SQL;
	const DATA: DATA = await JSON.parse(fs.readFileSync('data/DATA.json', 'utf8')) as DATA;
	// eslint-disable-next-line no-console
	console.log('Done!');
	
	// make DB connection
	process.stdout.write('Connecting to Database... ');
	// Website database
	const conn = await mysql.createConnection(
		{
			host: SQL.host,
			user: SQL.user,
			password: SQL.password,
			database: SQL.database
		});
	try
	{
		void conn.connect();
	}
	catch(err)
	{
		// eslint-disable-next-line no-console
		console.log(err);
		process.exit(1);
	}
	const acc_conn = await mysql.createConnection(
		{
			host: SQL.host,
			user: SQL.user,
			password: SQL.password,
			database: SQL.acc_database
		});
	try
	{
		void acc_conn.connect();
	}
	catch(err)
	{
		// eslint-disable-next-line no-console
		console.log(err);
		process.exit(1);
	}
	// eslint-disable-next-line no-console
	console.log('Done!');
	
	// express session with MariaDB store
	process.stdout.write('Initializing Session Storage... ');
	const sessionStore = new MySQLStore({}, acc_conn);
	// initializing express session
	app.use(session(
		{
			key: 'uwu',
			secret: DATA.session.secret, // random string to encrypt the cookie value
			store: sessionStore,
			resave: false,
			saveUninitialized: true,
			maxAge: 2592000000, // login status kept
			cookie:
				{
					domain: '.hohs.me',
					maxAge: 2592000000 // cookie kept
				}
		} as session.SessionOptions));
	// eslint-disable-next-line no-console
	console.log('Done!');

	// initialize passportjs
	app.use(passport.initialize());
	app.use(passport.session());
	passport.use(new GoogleStrategy(
		{
			clientID: DATA.google.clientID,
			clientSecret: DATA.google.clientSecret,
			userProfileURL: 'https://www.googleapis.com/oauth2/v3/userinfo', // workaround for forcing passport to use the new google signin api
			callbackURL: '/auth/google/callback'
		},
		// eslint-disable-next-line @typescript-eslint/no-misused-promises
		async (accessToken, refreshToken, profile, cb) => 
		{
			// eslint-disable-next-line
			const domain = profile._json.hd; // google account domain for managed user accounts

			if(domain === 'hcpss.org') // if user is teacher
			{
				try
				{
					const googleID = profile.id; // google provided UUID
					const [[user]] = await acc_conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `teachers` WHERE `googleID` = ?', [googleID]);
					if(user) return cb('', user); 
					else // if it's user's first time logging in with google
					{
						const googleEmail = profile.emails![0].value; // 1st email associated with that google account
						const [username] = googleEmail.split('@', 1); // username retrived from email
						const [[user]] = await acc_conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `teachers` WHERE LCASE(username) LIKE LCASE(?)', [username]); // get user with the matching username
						if(user) // if user with the matching username is found
						{
							const firstName = profile.name!.givenName; // first name retrived from google
							const lastName = profile.name!.familyName; // last name retrived from google
							await acc_conn.execute('UPDATE `teachers` SET `googleID` = ?, `firstName` = ?, `lastName` = ?, `email` = ? WHERE `username` = ?',
								[googleID, firstName, lastName, googleEmail, username]); // update DB with info reterived from Google
							const [[user]] = await acc_conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `teachers` WHERE `googleId` = ?', [googleID]); // find the user just added
							return cb('', user); // end login procedure
						}
						else return cb('', false, { message: 'It seems like you are not a registered as a Howard Teacher!<br>Are you new? Please contact to Mr.Zaron @ A200 to claim your account!' });
					}
				}
				catch(err)
				{
					return cb('', false, { message: '500 Internal Server Error X(<br>Please Retry!'});
				}
			}

			else if(domain === 'inst.hcpss.org') // if user is student
			{
				try
				{
					const googleID = profile.id; // user's google UUID
					const [[user]] = await acc_conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `students` WHERE `googleID` = ?', [googleID]); // get matching user with the UUID if found in the DB
					if(user) return cb('', user); // if matching user exists end the statement
					else // if it's user's first time logging in with google
					{
						const googleEmail = profile.emails![0].value; // 1st email associated with that google account
						const [username] = googleEmail.split('@', 1); // username retrived from email
						const [[user]] = await acc_conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `students` WHERE `username` = ?', [username]); // get user with the matching username
						if(user) // if user with the matching username is found
						{
							const firstName = profile.name!.givenName; // first name retrived from google
							const lastName = profile.name!.familyName; // last name retrived from google
							await acc_conn.execute('UPDATE `students` SET `googleID` = ?, `firstName` = ?, `lastName` = ?, `email` = ? WHERE `username` = ?',
								[googleID, firstName, lastName, googleEmail, username]); // update DB with info reterived from Google
							const [[user]] = await acc_conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `students` WHERE `googleId` = ?', [googleID]); // find the user just added
							return cb('', user); // end login procedure
						}
						else return cb('', false, { message: 'It seems like you are not a Howard student!<br>Are you new? Please contact to Mr.Zaron @ A200 to claim your account!' });
					}
				}
				catch(err)
				{
					return cb('', false, { message: '500 Internal Server Error X(<br>Please Retry!'});
				}
			}

			else return cb('', false, { message: 'It seems like the account you provided is not linked to HCPSS!<br>Are you using HCPSS provided Google Account?<br>Error Code: 401' });
		}));

	// invoked when user session is created (on login)
	// stringified data is inserted to session store and used as an user indentification data
	passport.serializeUser((user: Student|Teacher, done) =>
	{
		return done(null, JSON.stringify([user.googleID, user.accountLevel]));
	});

	// invoked whenever the server needs to fetch actual user data from data in the session store (so on every page hit, basically)
	// eslint-disable-next-line @typescript-eslint/no-misused-promises
	passport.deserializeUser(async (rawarray: string, done) =>
	{
		try
		{
			const array = JSON.parse(rawarray) as Array<number|string>;
			const googleID = array[0];
			const accountLevel = array[1];
			let realIdentity = 0;
			let [rows] = await acc_conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `students` WHERE `googleID` = ?', [googleID]);
			if(rows.length === 0) 
			{
				[rows] = await acc_conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `teachers` WHERE `googleID` = ?', [googleID]);
				realIdentity = 1;
			}
			rows[0].accountLevel = accountLevel; // get req.user.accountLevel from serialized data (will be later overwritten if needed)
			rows[0].realIdentity = realIdentity; // set req.user.realIdentity: 0 if user exists in students table, 1 if user exists in teacher table
			return done(null, rows[0]); // returns user object (can later be accessed from req.user)
		}
		catch(err)
		{
			return done(err);
		}
	});
	
	// Initializing General Variables
	
	// port
	const port = 8080;
	
	// Read settings from DB
	// settings is read as a general variable to reduce DB call (as they don't change that often anyways)
	process.stdout.write('Reading Settings from Database... ');
	const settings = await util.readSettings(conn) as Settings; // await - the function calls DB inside
	// eslint-disable-next-line no-console
	console.log('Done!');
	
	// Routing
	// Robots.txt - prevents search engine crawling
	// Google it for more information
	app.get('/robots.txt', (req, res) =>
	{
		res.type('text/plain'); // serving in text file format
		res.send('User-agent: *\n' +
			'Disallow: /auth/\n' +
			'Disallow: /mgmt/\n');
	});
	
	// Dynamic Routes
	const auth = Routes.auth(passport); // passing down connections
	app.use('/auth', auth);
	const mgmt = Routes.mgmt(settings, acc_conn, conn);
	app.use('/mgmt', mgmt);
	const pass = Routes.pass(settings, acc_conn, conn);
	app.use('/pass', pass);
	const root = Routes.root(settings, acc_conn, conn); // root should always go on the bottom
	app.use('/', root);
	
	// Static Routes
	// serving everything under public folder
	// css, js, you name it!
	app.use(express.static('public'));

	// handling 404
	app.get('*', (req, res) =>
	{
		res.status(404);
		res.sendFile('public/img/404.png', { root: __dirname });
	});
	
	// Starting Server
	app.listen(port, () =>
	{
		// eslint-disable-next-line no-console
		console.log(`The server is ready! Running on http://localhost:${port}`);
	});
	
	// Cron Jobs
	// Note: refer to the documentation on the npmjs.com
	// This cronjob module do not support weekday 7 - use 0
	// to keep the DB connection alive (every hour)
	// eslint-disable-next-line @typescript-eslint/no-misused-promises
	(new CronJob('0 0 * * * *', async () =>
	{
		let errCount = 0;
		while(errCount < 10)
		{
			try
			{
				await conn.execute('SELECT 1');
				await acc_conn.execute('SELECT 1');
				break;
			}
			catch(err)
			{
				errCount++;
			}
		}
	})).start();

	// on 14:10 the day before
	// eslint-disable-next-line @typescript-eslint/no-misused-promises
	(new CronJob(`0 ${settings.schoolEndMins} ${settings.schoolEndHrs} * * ${(parseInt(settings.day) + 6) % 7}`, async () =>
	{
		let errCount = 0;
		while(errCount < 10)
		{
			try
			{
				await conn.execute('UPDATE `params` SET `value`=? WHERE `key`=?', ['true', 'enabled']); // set enabled to string 'false'
				break;
			}
			catch(err)
			{
				errCount++;
			}
		}
		// eslint-disable-next-line no-console
		console.log(new Date().toDateString() + ' ' + new Date().toTimeString(), 'Signups Closed');
		// TODO: email error to admin if errCount exceeds 10
	})).start();
	
	// on 14:10 the day
	// eslint-disable-next-line @typescript-eslint/no-misused-promises
	(new CronJob(`0 ${settings.schoolEndMins} ${settings.schoolEndHrs} * * ${settings.day}`, async () =>
	{
		let errCount = 0;
		while(errCount < 10)
		{
			try
			{
				await conn.execute('UPDATE `params` SET `value`=? WHERE `key`=?', ['true', 'enabled']); // set enabled to string 'true'
				break;
			}
			catch(err)
			{
				errCount++;
			}
		}
		
		try
		{
			const params = await util.readParams(conn);
			void conn.execute('UPDATE `params` SET `value`=? WHERE `key`=?', [parseInt(params.week) + 1, 'week']); // add one to 'week'
		}
		catch(err)
		{
			// TODO: email error to admin (we don't want the repeat method to increase the week value by 2 or more)
		}
		// eslint-disable-next-line no-console
		console.log(new Date().toDateString() + ' ' + new Date().toTimeString(), 'Signups Closed');
	})).start();
}

void main(); // run the main function