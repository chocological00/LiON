/* Initializing dependencies */
import express from 'express';
const route = express.Router();

import mysql from 'mysql2/promise';

import * as util from '../util';
import { Settings, ReqUser } from '../types';

const rootHandler = (settings: Settings, acc_conn: mysql.Connection, conn: mysql.Connection): express.Router =>
{
	/* Initializing general variables */
	// Default variable set for layout.pug
	const def = { title: settings.name };
	
	/* Routing */
	route.get('/', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) // if not logged in
		{
			const variables = // variables for pug
				{
					// error message (used for error redirects)
					error: req.flash('error'), // eslint-disable-line @typescript-eslint/no-unsafe-assignment
					// success message (used for success redirects)
					success: req.flash('success') // eslint-disable-line @typescript-eslint/no-unsafe-assignment
				};
			if(util.isMobile(req.headers['user-agent']!)) return res.render('mobile/root/login', Object.assign({}, def, variables));
			else return res.render('pc/root/login', Object.assign({}, def, variables)); // render login page if not logged in
		}
		
		if((req.user as ReqUser).accountLevel === 0) // if user is a student
		{
			const variables =
				{
					name: (req.user as ReqUser).firstName, // firstname for welcome message
					username: (req.user as ReqUser).username,
					displayname: (req.user as ReqUser).firstName! + ' ' + (req.user as ReqUser).lastName!,
					navBar: true, // show or hide navigation bar
					signups: [{}], // my passes list
					status: false, // if signup is opened or not
					activityname: settings.name,
					weekday: settings.weekday, // string weekday of the activity
					error: req.flash('error'), // eslint-disable-line @typescript-eslint/no-unsafe-assignment
					success: req.flash('success') // eslint-disable-line @typescript-eslint/no-unsafe-assignment
				};
			
			// if signup is opened set pug variable to true
			const params = await util.readParams(conn);
			if(params.enabled) variables.status = true;
			
			// add this week's passes
			try
			{
				const [rows] = (await conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `passes` WHERE `username` = ? AND `week` = ?', [(req.user as ReqUser).username, params.week]));
				for(let c = 0; c < rows.length; c++) // for each rows
				{
					const row = rows[c];
					const teacher_uname = row.teacher; // eslint-disable-line @typescript-eslint/no-unsafe-assignment
					const [[teacherrow]] = await acc_conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `teachers` WHERE `username` = ?', [teacher_uname]); // get teacher info with matching username
					variables.signups.push({id: row.id, teacherName: `${teacherrow.firstName as string} ${teacherrow.lastName as string}`, room: teacherrow.room, purpose: row.purpose,time: row.time}); // eslint-disable-line @typescript-eslint/no-unsafe-assignment
				}
				
				if(util.isMobile(req.headers['user-agent']!)) return res.render('mobile/root/index_student', Object.assign({}, def, variables));
				else return res.render('pc/root/index_student', Object.assign({}, def, variables));
			}
			catch(err)
			{
				req.flash('error', '500 Internal Server Error X(');
				return res.redirect('/auth/logout'); // since all DB errors comes here by default, if an error continues to occur it would be reasonable to log them out
			}
		}
		else if((req.user as ReqUser).accountLevel === 1) // if user is a teacher
		{
			if(!(req.user as ReqUser).room)
			{
				req.flash('success', 'It seems like this is your first time logging in!<br>Please fill out the form below to continue :D');
				res.redirect('/settings');
			}

			const variables =
				{
					name: (req.user as ReqUser).firstName! + ' ' + (req.user as ReqUser).lastName!, // set the name
					username: (req.user as ReqUser).username,
					displayname: (req.user as ReqUser).firstName! + ' ' + (req.user as ReqUser).lastName!,
					navBar: true,
					signups: [{}], // passes with destination to this room
					status: 'Closed', // for teachers and admins, status is string for frontend code convenience. I know this is not a good design. Stop complaining.
					weekday: settings.weekday,
					error: req.flash('error'), // eslint-disable-line @typescript-eslint/no-unsafe-assignment
					success: req.flash('success') // eslint-disable-line @typescript-eslint/no-unsafe-assignment
				};
			
			// if signup is opened set pug variable to 'Opened'
			const params = await util.readParams(conn);
			if(params.enabled) variables.status = 'Opened';
			
			// add this week's passes with the correct destination
			try
			{
				const [rows] = (await conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `passes` WHERE `teacher` = ? AND `week` = ?', [(req.user as ReqUser).username, params.week]));
				for(let c = 0; c < rows.length; c++)
				{
					const row = rows[c];
					const student_uname = row.username; // eslint-disable-line @typescript-eslint/no-unsafe-assignment
					const [[userrow]] = await acc_conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `students` WHERE `username` = ?', [student_uname]); // get student info with matching student username
					variables.signups.push({id: row.id, studentName: `${userrow.firstName as string} ${userrow.lastName as string}`, purpose: row.purpose}); // eslint-disable-line @typescript-eslint/no-unsafe-assignment
				}
				
				return res.render('pc/root/index_teacher', Object.assign({}, def, variables));
			}
			catch(err)
			{
				req.flash('error', '500 Internal Server Error X(');
				return res.redirect('/auth/logout');
			}
		}
		else if((req.user as ReqUser).accountLevel === 2) // if a user is an admin
		{
			const variables =
				{
					title: settings.name + ' Management Panel',
					name: (req.user as ReqUser).firstName! + ' ' + (req.user as ReqUser).lastName!,
					username: (req.user as ReqUser).username,
					displayname: (req.user as ReqUser).firstName! + ' ' + (req.user as ReqUser).lastName!,
					navBar: true,
					status: 'Closed', // same as that of room's
					weekday: settings.weekday,
					error: req.flash('error'), // eslint-disable-line @typescript-eslint/no-unsafe-assignment
					success: req.flash('success') // eslint-disable-line @typescript-eslint/no-unsafe-assignment
				};
			
			const params = await util.readParams(conn);
			if(params.enabled) variables.status = 'Opened';
			
			return res.render('pc/root/index_admin', Object.assign({}, def, variables));
		}
		else return res.redirect('/auth/logout'); // fallback code for situation when the session data is corrupt. shouldn't happen.
	});

	route.get('/settings', (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');

		if((req.user as ReqUser).accountLevel === 0)
		{	
			const variables = 
				{
					navBar: true,
					displayname: (req.user as ReqUser).firstName! + ' ' + (req.user as ReqUser).lastName!,
					name: (req.user as ReqUser).firstName,
					google: (req.user as ReqUser).googleID !== null,
					username: (req.user as ReqUser).username,
					firstName: (req.user as ReqUser).firstName,
					lastName: (req.user as ReqUser).lastName,
					email: (req.user as ReqUser).email,
					gradYear: (req.user as ReqUser).gradYear,
					error: req.flash('error'), // eslint-disable-line @typescript-eslint/no-unsafe-assignment
					success: req.flash('success') // eslint-disable-line @typescript-eslint/no-unsafe-assignment
				};

			return res.render('pc/root/settings_student.pug', Object.assign({}, def, variables));
		}
		else if((req.user as ReqUser).accountLevel === 1)
		{
			const variables = 
				{
					navBar: true,
					displayname: (req.user as ReqUser).firstName! + ' ' + (req.user as ReqUser).lastName!,
					name: (req.user as ReqUser).firstName,
					google: (req.user as ReqUser).googleID !== null,
					username: (req.user as ReqUser).username,
					firstName: (req.user as ReqUser).firstName,
					lastName: (req.user as ReqUser).lastName,
					email: (req.user as ReqUser).email,
					userdepts: (req.user as ReqUser).depts,
					depts: settings.depts,
					room: (req.user as ReqUser).room,
					maxStudents: (req.user as ReqUser).maxStudents,
					error: req.flash('error'), // eslint-disable-line @typescript-eslint/no-unsafe-assignment
					success: req.flash('success') // eslint-disable-line @typescript-eslint/no-unsafe-assignment
				};
			
			return res.render('pc/root/settings_teacher.pug', Object.assign({}, def, variables));
		}
		else return res.redirect('/');
	});

	route.post('/settings', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');

		if((req.user as ReqUser).accountLevel === 0)
		{
			if(!(req.body.firstName && req.body.lastName && req.body.email && req.body.gradYear)) // eslint-disable-line @typescript-eslint/no-unsafe-member-access
			{
				req.flash('error', 'Some fields are blank!');
				return res.redirect('/settings');
			}

			try
			{
				await acc_conn.execute<mysql.RowDataPacket[]>('UPDATE `students` SET `firstName` = ?, `lastName` = ?, `email` = ?, `gradYear` = ? WHERE `username` = ?',
					[req.body.firstName, req.body.lastName, req.body.email, req.body.gradYear, (req.user as ReqUser).username]); // eslint-disable-line @typescript-eslint/no-unsafe-member-access
				req.flash('success', 'Your personal settings have been successfully updated!');
				return res.redirect('/settings');
			}
			catch(err)
			{
				req.flash('error', '500 Internal Server Error');
				return res.redirect('/');
			}
		}
		else if((req.user as ReqUser).accountLevel === 1)
		{
			if(!(req.body.firstName && req.body.lastName && req.body.email && req.body.depts && req.body.room && req.body.maxStudents)) // eslint-disable-line @typescript-eslint/no-unsafe-member-access
			{
				req.flash('error', 'Some fields are blank!');
				return res.redirect('/settings');
			}

			const depts = req.body.depts as Array<string>; // eslint-disable-line @typescript-eslint/no-unsafe-member-access

			const departments = [];
			for(let c = 0; c < depts.length; c++)
			{
				if(!depts[c].startsWith('Department'))
				{
					let alreadyExists = false;
					for(let i = 0; i < departments.length; i++)
					{
						if(departments[i] === depts[c]) alreadyExists = true;
					}
					if(!alreadyExists) departments.push(depts[c]);
				}
			}

			try
			{
				await acc_conn.execute<mysql.RowDataPacket[]>('UPDATE `teachers` SET `firstName` = ?, `lastName` = ?, `email` = ?, `depts` = ?, `room` = ?, `maxStudents` = ? WHERE `username` = ?',
					[req.body.firstName, req.body.lastName, req.body.email, JSON.stringify(departments), req.body.room, req.body.maxStudents, (req.user as ReqUser).username]); // eslint-disable-line @typescript-eslint/no-unsafe-member-access
				req.flash('success', 'Your personal settings have been successfully updated!<br><a href="/">Click Here to return to the front page.</a>');
				return res.redirect('/settings');
			}
			catch(err)
			{
				req.flash('error', '500 Internal Server Error X(');
				return res.redirect('/');
			}
		}
		else if((req.user as ReqUser).accountLevel === 2) res.redirect('/');

		else return res.redirect('/auth/logout');
	});
	
	route.get('/aboutus', (req, res) =>
	{
		// TODO: make aboutus page
		return res.send('Developed originally by David Kim\nCurrently Under JT Campbell\'s Management');

	});
	
	return route;
};

export default rootHandler;