import auth from './auth';
import mgmt from './mgmt';
import pass from './pass';
import root from './root';

const Routes =
{
	auth,
	mgmt,
	pass,
	root
};

export default Routes;