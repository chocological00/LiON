/* Initializing dependencies */
import express from 'express';
const route = express.Router();

import mysql from 'mysql2/promise';

import * as util from '../util';
import { Settings, ReqUser } from '../types';

const passHandler = (settings: Settings, acc_conn: mysql.Connection, conn: mysql.Connection): express.Router =>
{
	const def = { title: settings.name };
	
	// pass signup page
	route.get('/signup', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if((req.user as ReqUser).accountLevel !== 0) return res.redirect('/'); // redirect to '/' if not a student
		
		const params = await util.readParams(conn); // read parameters from DB
		if(params.enabled) // if signups are opened
		{
			const variables = // variables to pass down to pug renderer
				{
					navBar: true, // show navBar
					error: req.flash('error'), // eslint-disable-line @typescript-eslint/no-unsafe-assignment
					success: req.flash('success'), // eslint-disable-line @typescript-eslint/no-unsafe-assignment
					displayname: (req.user as ReqUser).firstName! + ' ' + (req.user as ReqUser).lastName!, // user's name to display on top navbar
					purposes: settings.purposes, // available purposes from settings
					depts: await util.getDepartments(conn) // get all the departments and save it to variables
				};
			if(util.isMobile(req.headers['user-agent']!)) return res.render('mobile/pass/signup', Object.assign({}, def, variables));
			else return res.render('pc/pass/signup', Object.assign({}, def, variables));
		}
		else
		{
			req.flash('error', 'Signups are not opened!');
			return res.redirect('/');
		}
	});
	
	route.post('/signup', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if((req.user as ReqUser).accountLevel !== 0) return res.redirect('/');

		const username = (req.user as ReqUser).username;
		const teacher = req.body.teacher as string; // eslint-disable-line @typescript-eslint/no-unsafe-member-access
		const purpose = req.body.purpose as string; // eslint-disable-line @typescript-eslint/no-unsafe-member-access
		const params = await util.readParams(conn);
		if(params.enabled) // if signups are enabled
		{
			try
			{
				// if pass to that teacher doesn't exist for this week (double clicking the signup button may send multiple signup requests for one teacher)
				const alreadySignedUp = ((await conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `passes` WHERE `username`=? AND `teacher`=? AND `week`=?', [username, teacher, params.week]))[0]).length > 0;
				const teacherFull = ((await conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `passes` WHERE `teacher`=? AND `week`=?', [teacher, params.week]))[0]).length >= parseInt((await acc_conn.execute<mysql.RowDataPacket[]>('SELECT `maxStudents` FROM `teachers` WHERE `username` = ?', [teacher]))[0][0].maxStudents); 
				if(teacherFull)
				{
					req.flash('error', 'The teacher you requested is full! :(');
					return res.redirect('/');
				}
				if(!(alreadySignedUp||teacherFull))
				{
					const date = new Date(); // make current date
					const dateString = date.toDateString() + ' ' + date.toTimeString(); // date string (can be parsed using date function again. refer to MDN)
					// put a pass into DB
					await conn.execute<mysql.RowDataPacket[]>('INSERT INTO `passes` (`username`, `teacher`, `purpose`, `time`, `week`) VALUES (?, ?, ?, ?, ?)', [username, teacher, purpose, dateString, params.week]);
				}
				req.flash('success', 'Successfully signed up!');
				return res.redirect('/');
			}
			catch(err)
			{
				req.flash('error', '500 Internal Server Error X(');
				return res.redirect('/');
			}
		}
		else
		{
			req.flash('error', 'Signups are not opened!');
			return res.redirect('/');
		}
	});
	
	// AJAX. Grab available teachers
	route.get('/available_teachers', async (req, res) =>
	{
		if(!req.query.hasOwnProperty('dept')) return res.send(''); // if dept is undefined. DO NOT redirect as this whole page is just for AJAX purposes
		if(req.query.dept === 'Choose an option') return res.send(''); // if dept is not given send blank data

		const params = await util.readParams(conn);

		let errCount = 0;
		while(errCount < 2)
		{
			try
			{
				// SQL - select all teachers with dept (json format) and order it alphabetically
				// Compitable with MariaDB 10.2 or higher
				// Search string for json should be wrapped in ""
				const [rows] = await acc_conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `teachers` WHERE JSON_CONTAINS(`depts`, ?) ORDER BY `lastName` ASC', [`"${req.query.dept as string}"`]);

				const teachers = [];
				for(let c = 0; c < rows.length; c++) // for each rows found
				{
					const row = rows[c];
					teachers.push([row.username, `${row.firstName as string} ${row.lastName as string}`, row.room]); // push an (array with teacher username and teacher name) in 'teacher' array
				}
				
				let response = '';
				for(let c = 0; c < teachers.length; c++) // for each elements in teachers array
				{
					const teacherFull = ((await conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `passes` WHERE `teacher`=? AND `week`=?', [teachers[c][0], params.week]))[0]).length >= parseInt((await acc_conn.execute<mysql.RowDataPacket[]>('SELECT `maxStudents` FROM `teachers` WHERE `username` = ?', [teachers[c][0]]))[0][0].maxStudents); 
					
					response += `<option value="${teachers[c][0] as string}"${teacherFull ? ' disabled': ''}>${teachers[c][1] as string} - ${teachers[c][2] as string}${teacherFull ? ' (Full)': ''}</option>`; // value is teacher username and innerHTML is teacher name - room number
				}

				return res.send(response); // send response
			}
			catch(err)
			{
				errCount++;
			}
		}
		return res.send(''); // This is AJAX so if sth goes wrong, reasonable users would just refresh the page
	});
	
	// Delete pass
	route.get('/delete_pass', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if((req.user as ReqUser).accountLevel !== 0) return res.redirect('/'); // if not student redirect to '/'
		else
		{
			try
			{
				const passid = req.query.passid; // get pass number
				const username = (req.user as ReqUser).username;
				await conn.execute<mysql.RowDataPacket[]>('DELETE FROM `passes` WHERE `id` = ? AND `username` = ?', [passid, username]); // if a person tries to delete a pass that is not his/hers, nothing will happen
				req.flash('success', 'Pass Deleted!');
				return res.redirect('/');
			}
			catch(err)
			{
				req.flash('Something went wrong X(<br>Please try again!');
				return res.redirect('/');
			}
		}
	});
	
	return route;
};

export default passHandler;