/* Initializing dependencies */
import express from 'express';
const route = express.Router();

import mysql from 'mysql2/promise';

import * as util from '../util';
import { Settings, ReqUser } from '../types';

const mgmtHandler = (settings: Settings, acc_conn: mysql.Connection, conn: mysql.Connection): express.Router =>
{
	/* Initializing general variables */
	// Default variable set for layout.pug
	const def = { title: settings.name + ' Management Panel' };
	
	/* Routing */

	// allow admins to sign up for passes
	route.get('/test_other_view', (req, res) => 
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if((req.user as ReqUser).accountLevel !== 2) return res.redirect('/');

		// temporaily serialized session data to trick browser into thinking that the user is a student, not an admin
		// horrible design, but I was busy and wanted a minimal fix
		// see index.ts > main > passport.deserializeUser
		const userObj = JSON.parse(req.session!.passport.user as string) as Array<string|number>; // eslint-disable-line @typescript-eslint/no-unsafe-member-access
		userObj[1] = (req.user as ReqUser).realIdentity ;
		req.session!.passport.user = JSON.stringify(userObj); // eslint-disable-line @typescript-eslint/no-unsafe-member-access

		return res.redirect('/');
	});

	// adding one user
	route.get('/add_user', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/'); // if not logged in
		if((req.user as ReqUser).accountLevel !== 2) return res.redirect('/'); // if not admin, redirect
		
		const depts = await util.getDepartments(conn);
		const variables =
			{
				username: (req.user as ReqUser).username,
				displayname: `${(req.user as ReqUser).firstName!} ${(req.user as ReqUser).lastName!}`,
				navBar: true,
				error: req.flash('error'), // eslint-disable-line @typescript-eslint/no-unsafe-assignment
				success: req.flash('success'), // eslint-disable-line @typescript-eslint/no-unsafe-assignment
				depts: JSON.stringify(depts)
			};
		
		return res.render('pc/mgmt/add_user', Object.assign({}, def, variables));
	});
	
	route.post('/add_user', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if((req.user as ReqUser).accountLevel !== 2) return res.redirect('/');
		
		const body = req.body as { [key: string]: string };

		if(!(body.hasOwnProperty('accountLevel') && body.hasOwnProperty('username')))
		{
			req.flash('error', 'Some fields are blank!');
			return res.redirect('/mgmt/add_user');
		}
		const accountLevel = parseInt(body.accountLevel);
		const username = body.username.trim();
		let firstName;
		if(body.hasOwnProperty('firstName')) firstName = body.firstName.trim();
		let lastName;
		if(body.hasOwnProperty('lastName')) lastName = body.lastName.trim();
		let gradYear;
		if(body.hasOwnProperty('gradYear')) gradYear = body.gradYear; // student only
		let depts;
		if(body.hasOwnProperty('depts')) depts = body.depts; // teachers only
		let maxStudents;
		if(body.hasOwnProperty('maxStudents')) maxStudents = body.maxStudents; // teachers only
		let studentOrTeacher;
		if(body.hasOwnProperty('studentOrTeacher')) studentOrTeacher = body.studentOrTeacher;

		switch(accountLevel) 
		{
		case 0:
		{
			// check if any fields are blank
			if (!(username && firstName && lastName && gradYear))
			{
				req.flash('error', 'Some fields are blank!');
				return res.redirect('/mgmt/add_user');
			}
			try
			{
				// prepared statements - automatically escapes certain characters to prevent SQL injection
				const [rows] = await acc_conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `students` WHERE `username`=?', [username]); // search for that username first
				if (rows.length > 0) // if that username exists redirect with error
				{
					req.flash('error', 'Username already exists in students\' database!');
					return res.redirect('/mgmt/add_user');
				}
				else
				{
					// add that user
					await acc_conn.execute<mysql.RowDataPacket[]>('INSERT INTO `students` (`username`, `firstName`, `lastName`, `gradYear`) VALUES ' +
						'(?, ?, ?, ?)', [username, firstName, lastName, gradYear]);
					// redirect with success
					req.flash('success', `Student ${username} added successfully!`);
					return res.redirect('/mgmt/add_user');
				}
			}
			catch(err)
			{
				req.flash('error', '500 Internal Server Error X(');
				return res.redirect('/');
			}
		}
			
		case 1:
		{
			if (!(username && firstName && lastName && maxStudents && depts))
			{
				req.flash('error', 'Some fields are blank!');
				return res.redirect('/mgmt/add_user');
			}
			
			try
			{
				const departments = [];
				for(let c = 0; c < depts.length; c++)
				{
					if(!depts[c].startsWith('Department'))
					{
						let alreadyExists = false;
						for(let i = 0; i < departments.length; i++)
						{
							if(departments[i] === depts[c]) alreadyExists = true;
						}
						if(!alreadyExists) departments.push(depts[c]);
					}
				}

				const [rows] = await acc_conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `teachers` WHERE `username`=?', [username]);

				if (rows.length > 0) // if that username exists redirect with error
				{
					req.flash('error', 'Username already exists in teachers\' database!');
					return res.redirect('/mgmt/add_user');
				}
				else
				{
					await acc_conn.execute<mysql.RowDataPacket[]>('INSERT INTO `teachers` (`username`, `firstName`, `lastName`, `depts`, `maxStudents`) VALUES ' +
						'(?, ?, ?, ?, ?)', [username, firstName, lastName, JSON.stringify(departments), maxStudents]);
					// redirect with success
					req.flash('success', `Room account ${username} added successfully!`);
					return res.redirect('/mgmt/add_user');
				}
			}
			catch(err)
			{
				req.flash('error', '500 Internal Server Error X(');
				return res.redirect('/');
			}
		}
			
		case 2:
		{
			if (!(username && studentOrTeacher))
			{
				req.flash('error', 'Some fields are blank!');
				return res.redirect('/mgmt/add_user');
			}
			
			try
			{
				studentOrTeacher = parseInt(studentOrTeacher);
				if(studentOrTeacher === 0)
				{
					const [rows] = await acc_conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `students` WHERE `username`=?', [username]);

					if(rows.length === 0)
					{
						req.flash('error', 'User not found in Student\'s database!');
						return res.redirect('/mgmt/add_user');
					}
					else
					{
						await acc_conn.execute<mysql.RowDataPacket[]>('UPDATE `students` SET `accountLevel` = ? WHERE `username` = ?', [2, username]);
						req.flash('success', `Account ${username} promoted successfully!`);
						return res.redirect('/mgmt/add_user');
					}
				}
				else if(studentOrTeacher === 1)
				{
					const [rows] = await acc_conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `teachers` WHERE `username`=?', [username]);

					if(rows.length === 0)
					{
						req.flash('error', 'User not found in teacher\'s database!');
						return res.redirect('/mgmt/add_user');
					}
					else
					{
						await acc_conn.execute<mysql.RowDataPacket[]>('UPDATE `teachers` SET `accountLevel` = ? WHERE `username` = ?', [2, username]);
						req.flash('success', `Account ${username} promoted successfully!`);
						return res.redirect('/mgmt/add_user');
					}
				}
				else 
				{
					req.flash('error', '500 Internal Server Error');
					return res.redirect('/mgmt/add_user');
				}
			}
			catch(err)
			{
				req.flash('error', '500 Internal Server Error X(');
				return res.redirect('/');
			}
		}

		default:
			req.flash('error', '500 Internal Server Error');
			return res.redirect('/mgmt/add_user');
		}
		
	});
	
	// adding multiple users
	route.get('/add_users', (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if((req.user as ReqUser).accountLevel !== 2) return res.redirect('/');
		
		const variables =
			{
				username: (req.user as ReqUser).username,
				displayname: (req.user as ReqUser).firstName! + ' ' + (req.user as ReqUser).lastName!,
				navBar: true,
				error: req.flash('error'), // eslint-disable-line @typescript-eslint/no-unsafe-assignment
				success: req.flash('success') // eslint-disable-line @typescript-eslint/no-unsafe-assignment
			};
		return res.render('pc/mgmt/add_users', Object.assign({}, def, variables));
	});
	
	route.post('/add_users', (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if((req.user as ReqUser).accountLevel !== 2) return res.redirect('/');
		
		const field = req.body.field as string|undefined; // eslint-disable-line @typescript-eslint/no-unsafe-member-access

		if(!field)
		{
			req.flash('error', 'The field is blank!');
			return res.redirect('/mgmt/add_users');
		}
		
		const lines = field.split('\n'); // split by line breaks and make an array
		for(let c = 0; c < lines.length; c++) // for each lines
		{
			const words = lines[c].split(','); // split by comma and make an array
			for(let i = 0; i < lines[c].length; i++) // for each elements
			{
				words[i] = words[i].trim(); // lines is now a 2D array. inner array contains one line information separated by comma
			}
		}
		
		let errCount = 0; // counter for parse error - does not handle DB error
		for(let c = 0; c < lines.length; c++) // for each lines
		{
			const line = lines[c];
			
			const username = line[0];
			const lastName = line[1];
			const firstName = line[2];
			const gradYear = line[3];
			
			if(username&&lastName&&firstName&&gradYear) // if parsed correctly
			{
				// concurrent execution
				try
				{
					void acc_conn.execute<mysql.RowDataPacket[]>('INSERT INTO `students` (`username`, `firstName`, `lastName`, `gradYear`) VALUES (?, ?, ?, ?)',
						[username, firstName, lastName, gradYear]);
				}
				catch(err)
				{
					errCount++; // DB is executed concurrently, so this might have race condition
				}
			}
			else errCount++; // if not parsed correctly increase the count
		}

		if(errCount) // if error exists
		{
			req.flash('error', `Error parsing ${errCount} line(s). Check the database manually.`);
			return res.redirect('/mgmt/add_users');
		}
		else
		{
			req.flash('error', 'Processing started! Please allow some time for the server to finish adding accounts!');
			return res.redirect('/mgmt/add_users');
		}
	});
	
	// Change website settings
	route.get('/settings', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if((req.user as ReqUser).accountLevel !== 2) return res.redirect('/');
		
		try
		{
			const variables =
			{
				username: (req.user as ReqUser).username,
				displayname: (req.user as ReqUser).firstName! + ' ' + (req.user as ReqUser).lastName!,
				navBar: true,
				error: req.flash('error'), // eslint-disable-line @typescript-eslint/no-unsafe-assignment
				success: req.flash('success'), // eslint-disable-line @typescript-eslint/no-unsafe-assignment
				rows: (await conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `settings`'))[0]
			};
			return res.render('pc/mgmt/settings', Object.assign({}, def, variables));
		}
		catch(err)
		{
			req.flash('error', '500 Internal Server Error X(');
			return res.redirect('/');
		}
	});
	
	route.post('/settings', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if((req.user as ReqUser).accountLevel !== 2) return res.redirect('/');
		for(const key in req.body)
		{
			try
			{
				if(key === 'submit') continue; // skip the submit button
				// following code is potentially vulnerable, but since this API requires admin authentication anyways, it shouldn't matter
				await conn.execute<mysql.RowDataPacket[]>('UPDATE `settings` SET `value` = ? WHERE `key` = ?', [req.body[key], key]); // eslint-disable-line @typescript-eslint/no-unsafe-member-access
			}
			catch(err)
			{
				req.flash('error', '500 Internal server Error X(');
				return res.redirect('/');
			}
		}
		req.flash('success', 'Settings successfully updated! The website will be restarted in 10 seconds...');
		res.redirect('/mgmt/settings'); // no return here - the code below has to run
		setTimeout(() => { process.exit(); }, 10000); // process MUST be restarted from external task manager (sth like pm2 or forever)
	});
	
	// statistics page
	route.get('/stats', (req, res) =>
	{
		// TODO: write stats page
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if((req.user as ReqUser).accountLevel !== 2) return res.redirect('/');
		return res.render('pc/mgmt/stats', Object.assign({}, def, {}));
	});
	
	// change signup open status
	route.get('/change_status', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if((req.user as ReqUser).accountLevel !== 2) return res.redirect('/');

		const variables =
			{
				username: (req.user as ReqUser).username,
				displayname: (req.user as ReqUser).firstName! + ' ' + (req.user as ReqUser).lastName!,
				navBar: true,
				error: req.flash('error'), // eslint-disable-line @typescript-eslint/no-unsafe-assignment
				success: req.flash('success'), // eslint-disable-line @typescript-eslint/no-unsafe-assignment
				weekday: settings.weekday,
				status: await util.readParams(conn) ? 'enabled' : 'disabled' // ternary operator
			};
		return res.render('pc/mgmt/change_status', Object.assign({}, def, variables));
	});
	
	route.post('/change_status', async (req, res) =>
	{
		if(!req.hasOwnProperty('user')) return res.redirect('/');
		if((req.user as ReqUser).accountLevel !== 2) return res.redirect('/');
		
		if(req.body.change === 'change') // eslint-disable-line @typescript-eslint/no-unsafe-member-access
		{
			const params = await util.readParams(conn);
			try
			{
				if(params.enabled) // if current status is enabled
				{
					await conn.execute<mysql.RowDataPacket[]>('UPDATE `params` SET `value` = ? WHERE `key` = ?', ['false', 'enabled']);
					req.flash('success', `Signups are closed until closest ${settings.weekday}!`);
					return res.redirect('/mgmt/change_status');
				}
				else
				{
					await conn.execute<mysql.RowDataPacket[]>('UPDATE `params` SET `value` = ? WHERE `key` = ?', ['true', 'enabled']);
					req.flash('success', `Signups are opened until closest day before ${settings.weekday}!`);
					return res.redirect('/mgmt/change_status');
				}
			}
			catch(err)
			{
				req.flash('error', '500 Internal Server Error X(');
				return res.redirect('/');
			}
		}
		else
		{
			req.flash('error', 'Internal Server Error!');
			return res.redirect('/mgmt/change_status');
		}
	});
	
	return route;
};

export default mgmtHandler;