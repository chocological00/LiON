/* Initializing dependencies */
import express from 'express';
import { PassportStatic } from 'passport';
const route = express.Router();

const authHandler = (passport: PassportStatic): express.Router =>
{
	// google fed auth
	route.get('/google',
		passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/userinfo.profile'] })
	);

	route.get('/google/callback',
		passport.authenticate('google', { failureRedirect: '/', failureFlash: true}),
		(req, res) =>
		{
			// auth success
			return res.redirect('/');
		}
	);

	// logout
	route.get('/logout', (req, res) =>
	{
		req.logout();
		res.clearCookie('nyan_cat'); // destroy cookie in client
		return res.redirect('/');
	});
	
	return route;
};

export default authHandler;