import mysql from 'mysql2/promise';
import MD from 'mobile-detect';
import { Settings, Params } from './types';

/**
 * reads settings table from DB
 * @param conn - connection object to main DB
 * @returns {Promise<Object>} - use await
 */
const readSettings = async (conn: mysql.Connection): Promise<Settings|null> =>
{
	let errCount = 0;
	while(errCount < 2)
	{
		try
		{
			const settings: { [key: string]: string } = {};
			const rows = (await conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `settings`'))[0];
			for(let c = 0; c < rows.length; c++)
			{
				const row = rows[c] as { key: string, value: string };
				settings[row.key] = row.value; // assigns settings.{key} to {value}
			}
		
			return {
				school: settings['school'],
				name: settings['name'],
				day: settings['day'],
				schoolEndHrs: settings['schoolEndHrs'],
				schoolEndMins: settings['schoolEndMins'],
				purposes: await JSON.parse(settings.purposes) as Array<string>, // purposes in DB is saved as stringified JSON so parse it
				depts: settings['depts'],
				weekday: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][parseInt(settings['day'])]
			};
		}
		catch(err)
		{
			errCount++;
		}
	}
	return null; // should I just kill the process here? (so it can be restarted by external process manager)
};

/**
 * reads params table from DB
 * @param conn - connection object to main DB
 * @returns {Promise<Object>} - use await
 */
const readParams = async (conn: mysql.Connection): Promise<Params> =>
{
	let errCount = 0;
	while(errCount < 2)
	{
		try
		{
			const params: { [key: string]: string } = {};
			const rows = (await conn.execute<mysql.RowDataPacket[]>('SELECT * FROM `params`'))[0];
			for(let c = 0; c < rows.length; c++)
			{
				const row = rows[c] as { key: string, value: string };
				params[row.key] = row.value;
			}
			
			return {
				enabled: params.enabled === 'true',
				week: params.week
			};
		}
		catch(err)
		{
			errCount++;
		}
	}
	process.exit();
};

/**
 * get list of departments from DB
 * @param acc_conn - connection object to account DB
 * @returns {Promise<Array>} - string array of list of departments
 */
const getDepartments = async (conn: mysql.Connection): Promise<Array<string>|null> =>
{
	let errCount = 0;
	while(errCount < 2)
	{
		try
		{
			const [[row]] = await conn.execute<mysql.RowDataPacket[]>('SELECT `value` FROM `settings` WHERE `key` = ?', ['depts']);
			return await JSON.parse(row.value) as Array<string>;
		}
		catch(err)
		{
			errCount++;
		}
	}
	return null;
};

const isMobile = (UA: string): boolean =>
{
	const md = new MD(UA);
	// eslint-disable-next-line eqeqeq
	if((md.mobile()) && !(md.tablet())) return true;
	else return false;
};

export {
	readSettings,
	readParams,
	getDepartments,
	isMobile
};