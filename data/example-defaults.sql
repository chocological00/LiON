-- --------------------------------------------------------
-- 호스트:                          127.0.0.1
-- 서버 버전:                        10.3.10-MariaDB - mariadb.org binary distribution
-- 서버 OS:                        Win64
-- HeidiSQL 버전:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- accounts 데이터베이스 구조 내보내기
DROP DATABASE IF EXISTS `accounts`;
CREATE DATABASE IF NOT EXISTS `accounts` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `accounts`;

-- 테이블 accounts.sessions 구조 내보내기
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `expires` int(11) unsigned NOT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 accounts.sessions:~2 rows (대략적) 내보내기
DELETE FROM `sessions`;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;

-- 테이블 accounts.students 구조 내보내기
DROP TABLE IF EXISTS `students`;
CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `googleID` varchar(50) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `firstName` text DEFAULT NULL,
  `lastName` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `accountLevel` tinyint(1) NOT NULL DEFAULT 0,
  `gradYear` smallint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `googleID` (`googleID`)
) ENGINE=InnoDB AUTO_INCREMENT=1901 DEFAULT CHARSET=utf8;

-- 테이블 데이터 accounts.students:~1 rows (대략적) 내보내기
DELETE FROM `students`;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` (`id`, `googleID`, `username`, `firstName`, `lastName`, `email`, `accountLevel`, `gradYear`) VALUES
	(1, '117480402993086964904', 'dkim8420', 'David', 'Kim', 'dkim8420@inst.hcpss.org', 2, 2019);
/*!40000 ALTER TABLE `students` ENABLE KEYS */;

-- 테이블 accounts.teachers 구조 내보내기
DROP TABLE IF EXISTS `teachers`;
CREATE TABLE IF NOT EXISTS `teachers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `googleID` varchar(50) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `firstName` text DEFAULT NULL,
  `lastName` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `accountLevel` tinyint(1) NOT NULL DEFAULT 1,
  `depts` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '[]',
  `room` text DEFAULT NULL,
  `maxStudents` smallint(6) NOT NULL DEFAULT 20,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `googleID` (`googleID`)
) ENGINE=InnoDB AUTO_INCREMENT=196 DEFAULT CHARSET=utf8;

-- 테이블 데이터 accounts.teachers:~173 rows (대략적) 내보내기
DELETE FROM `teachers`;
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
INSERT INTO `teachers` (`id`, `googleID`, `username`, `firstName`, `lastName`, `email`, `accountLevel`, `depts`, `room`, `maxStudents`) VALUES
	(1, NULL, 'Nicholas_Novak', 'Nicholas', 'Novak', NULL, 1, '["Administration"]', NULL, 20),
	(2, NULL, 'Dale_Castro', 'Dale', 'Castro', NULL, 1, '["Administration"]', NULL, 20),
	(3, NULL, 'Shawn_Hastings', 'Shawn', 'Hastings-Hauf', NULL, 1, '["Administration"]', NULL, 20),
	(4, NULL, 'karen_mason', 'Karen', 'Mason', NULL, 1, '["Administration"]', NULL, 20),
	(5, NULL, 'Brian_Sackett', 'Brian', 'Sackett', NULL, 1, '["Administration"]', NULL, 20),
	(6, NULL, 'napoleon_saunders', 'Napoleon', 'Saunders II', NULL, 1, '["Administration"]', NULL, 20),
	(7, NULL, 'Michael_Duffy', 'Michael', 'Duffy', NULL, 1, '["Administration", "Athletics"]', NULL, 20),
	(8, NULL, 'Jody_Adams', 'Jody', 'Adams', NULL, 1, '["Administration"]', NULL, 20),
	(9, NULL, 'Theresa_Cleveland', 'Theresa', 'Cleveland', NULL, 1, '["Administration"]', NULL, 20),
	(10, NULL, 'kathrine_konarska', 'Kathrine', 'Konarska', NULL, 1, '["Administration"]', NULL, 20),
	(11, NULL, 'Despina_Mastrogianis', 'Despina', 'Mastrogianis', NULL, 1, '["Administration"]', NULL, 20),
	(12, NULL, 'maureen_taylor', 'Maureen', 'Taylor', NULL, 1, '["Administration"]', NULL, 20),
	(13, NULL, 'Leanne_Peters', 'Leanne', 'Peters', NULL, 1, '["Administration"]', NULL, 20),
	(14, NULL, 'Margaret_Miller', 'Margaret', 'Miller', NULL, 1, '["Administration"]', NULL, 20),
	(15, NULL, 'Jillian_Snyder', 'Jillian', 'Snyder', NULL, 1, '["Student Services"]', NULL, 20),
	(16, NULL, 'Matthew_Mindel', 'Matthew', 'Mindel', NULL, 1, '["Student Services"]', NULL, 20),
	(17, NULL, 'Shannon_Grieve', 'Shannon', 'Grieve', NULL, 1, '["Student Services"]', NULL, 20),
	(18, NULL, 'David_Glenn', 'David', 'Glenn', NULL, 1, '["Student Services"]', NULL, 20),
	(19, NULL, 'Melody_Green-Lewis', 'Melody', 'Green-Lewis', NULL, 1, '["Student Services"]', NULL, 20),
	(20, NULL, 'Sonya_Sutter', 'Sonya', 'Sutter', NULL, 1, '["Student Services"]', NULL, 20),
	(21, NULL, 'alexis_pappadeas', 'Alex', 'Pappedeas', NULL, 1, '["Student Services"]', NULL, 20),
	(22, NULL, 'Sharon_Flanagan', 'Sharon', 'Flanagan', NULL, 1, '["Student Services"]', NULL, 20),
	(23, NULL, 'Cristina_Deforge', 'Cristina', 'Deforge', NULL, 1, '["Student Services"]', NULL, 20),
	(24, NULL, 'MIke_Williams', 'Mike', 'Williams', NULL, 1, '["Student Services"]', NULL, 20),
	(25, NULL, 'Nicole_Nicholson', 'Nicole', 'Nicholson', NULL, 1, '["Student Services"]', NULL, 20),
	(26, NULL, 'katelyn_trinite', 'Katelyn', 'Trinite', NULL, 1, '["Student Services"]', NULL, 20),
	(27, NULL, 'Karen_Stark', 'Karen', 'Stark', NULL, 1, '["Student Services"]', NULL, 20),
	(28, NULL, 'Laura_Phebus', 'Laura', 'Phebus', NULL, 1, '["Student Services"]', NULL, 20),
	(29, NULL, 'Ronda_Lennon', 'Ronda', 'Lennon', NULL, 1, '["Student Services"]', NULL, 20),
	(30, NULL, 'isabel_sheeley', 'Isabel', 'Sheeley', NULL, 1, '["Student Services"]', NULL, 20),
	(31, NULL, 'jean_audain', 'Jean', 'Audain', NULL, 1, '["Student Services"]', NULL, 20),
	(32, NULL, 'Tyler_Wade', 'Tyler', 'Wade', NULL, 1, '["Student Services"]', NULL, 20),
	(33, NULL, 'michael_twardowicz', 'Mike', 'Twardowicz', NULL, 1, '["Student Services"]', NULL, 20),
	(34, NULL, 'hannah_kelly', 'Hannah', 'Kelly', NULL, 1, '["Art"]', NULL, 20),
	(35, NULL, 'Matthew_Hanson', 'Matthew', 'Hanson', NULL, 1, '["Art"]', NULL, 20),
	(36, NULL, 'Jeffrey_Hensley', 'Jeffrey', 'Hensley', NULL, 1, '["Art"]', NULL, 20),
	(37, NULL, 'Sarah_Hensley', 'Sarah', 'Hensley', NULL, 1, '["Art"]', NULL, 20),
	(39, NULL, 'Brad_Dyer', 'Brad', 'Dyer', NULL, 1, '["BCMS"]', NULL, 20),
	(40, NULL, 'chad_boothe', 'Chad', 'Boothe', NULL, 1, '["BCMS"]', NULL, 20),
	(41, NULL, 'jason_johnson1', 'Jason', 'Johnson', NULL, 1, '["BCMS", "Technology Ed"]', NULL, 20),
	(42, NULL, 'nils_schroder', 'Nils', 'Schroder', NULL, 1, '["BCMS", "Technology Ed"]', NULL, 20),
	(43, NULL, 'Nicholas_Zaron', 'Nicholas', 'Zaron', NULL, 1, '["BCMS"]', NULL, 20),
	(44, NULL, 'david_evans', 'David', 'Evans', NULL, 1, '["BSAP"]', NULL, 20),
	(45, NULL, 'Michael_Cohee', 'Michael', 'Cohee', NULL, 1, '["CRD"]', NULL, 20),
	(46, NULL, 'Laura_Parsons', 'Laura', 'Parsons', NULL, 1, '["CRD", "Special Education"]', NULL, 20),
	(47, NULL, 'Sara_Crisera', 'Sara', 'Crisera', NULL, 1, '["Dance"]', NULL, 20),
	(48, NULL, 'Jennifer_Dipietro', 'Jennifer', 'Dipietro', NULL, 1, '["Dance", "PE/Health"]', NULL, 20),
	(49, NULL, 'Erin_McArdle', 'Erin', 'McArdle', NULL, 1, '["English"]', NULL, 20),
	(50, NULL, 'Karina_Borkowski', 'Karina', 'Borkowski', NULL, 1, '["English"]', NULL, 20),
	(51, NULL, 'Christine_Boussy', 'Christine', 'Boussy', NULL, 1, '["English"]', NULL, 20),
	(52, NULL, 'Rebecca_Bragunier', 'Rebecca', 'Bragunier', NULL, 1, '["English"]', NULL, 20),
	(53, NULL, 'James_Burrows', 'James', 'Burrows', NULL, 1, '["English"]', NULL, 20),
	(54, NULL, 'Venice_Burrows', 'Venice', 'Burrows', NULL, 1, '["English"]', NULL, 20),
	(55, NULL, 'Julia_Carter', 'Julia', 'Carter', NULL, 1, '["English"]', NULL, 20),
	(56, NULL, 'Marsha_Demaree', 'Marsha', 'Demaree', NULL, 1, '["English"]', NULL, 20),
	(57, NULL, 'Matthew_Jens', 'Matthew', 'Jens', NULL, 1, '["English", "Special Education"]', NULL, 20),
	(58, NULL, 'Michael_Kaplan', 'Michael', 'Kaplan', NULL, 1, '["English", "Special Education"]', NULL, 20),
	(59, NULL, 'Markea_Kelly', 'Markea', 'Kelly', NULL, 1, '["English"]', NULL, 20),
	(60, NULL, 'Natasha_Lavoie', 'Natasha', 'Lavoie', NULL, 1, '["English"]', NULL, 20),
	(61, NULL, 'Colette_Lawrence', 'Colette', 'Lawrence', NULL, 1, '["English"]', NULL, 20),
	(62, NULL, 'Kristen_McManus', 'Kristen', 'McManus', NULL, 1, '["English"]', NULL, 20),
	(63, NULL, 'Margaret_Mitchell', 'Margaret', 'Mitchell', NULL, 1, '["English"]', NULL, 20),
	(64, NULL, 'Amanda_Orndorff', 'Amanda', 'Orndorff', NULL, 1, '["English"]', NULL, 20),
	(65, NULL, 'Shannon_Ryan', 'Shannon', 'Ryan', NULL, 1, '["English"]', NULL, 20),
	(66, NULL, 'Matthew_Stump', 'Matthew', 'Stump', NULL, 1, '["English"]', NULL, 20),
	(67, NULL, 'William_Tu', 'William', 'Tu', NULL, 1, '["English", "Special Education"]', NULL, 20),
	(68, NULL, 'Emily_Warner', 'Emily', 'Warner', NULL, 1, '["English"]', NULL, 20),
	(69, NULL, 'Lorna_Browne', 'Lorna', 'Browne', NULL, 1, '["FACS"]', NULL, 20),
	(70, NULL, 'Achint_Kaur', 'Achint', 'Kaur', NULL, 1, '["FACS", "Teacher Academy"]', NULL, 20),
	(71, NULL, 'Mary_Curtin', 'Mary', 'Curtin', NULL, 1, '["G/T", "Social Studies"]', NULL, 20),
	(72, NULL, 'Patricia_Marshall', 'Patricia', 'Marshall', NULL, 1, '["JROTC"]', NULL, 20),
	(73, NULL, 'Shawn_Boone', 'Shawn', 'Boone', NULL, 1, '["JROTC"]', NULL, 20),
	(74, NULL, 'mary_mohr', 'Mary', 'Mohr', NULL, 1, '["Media Center"]', NULL, 20),
	(75, NULL, 'heather_mccann', 'Heather', 'McCann', NULL, 1, '["Media Center"]', NULL, 20),
	(76, NULL, 'Charles_Boling', 'Charles', 'Boling', NULL, 1, '["Mathematics"]', NULL, 20),
	(77, NULL, 'kerianne_allen', 'Kerianne', 'Allen', NULL, 1, '["Mathematics"]', NULL, 20),
	(78, NULL, 'Mia_Ayres', 'Mia', 'Ayres', NULL, 1, '["Mathematics"]', NULL, 20),
	(79, NULL, 'Matthew_Braddock', 'Matthew', 'Braddock', NULL, 1, '["Mathematics"]', NULL, 20),
	(80, NULL, 'Nicole_Burns', 'Nicole', 'Burns', NULL, 1, '["Mathematics"]', NULL, 20),
	(81, NULL, 'Hedy_Cohen', 'Hedy', 'Cohen', NULL, 1, '["Mathematics"]', NULL, 20),
	(82, NULL, 'Alyson_Creighton', 'Alyson', 'Creighton', NULL, 1, '["Mathematics"]', NULL, 20),
	(83, NULL, 'Nikolett_DeVries', 'Nikolett', 'DeVries', NULL, 1, '["Mathematics"]', NULL, 20),
	(84, NULL, 'curtis_galbreath', 'Curtis', 'Galbreath', NULL, 1, '["Mathematics"]', NULL, 20),
	(85, NULL, 'Kyle_Janney', 'Kyle', 'Janney', NULL, 1, '["Mathematics"]', NULL, 20),
	(86, NULL, 'Sarah_Lim', 'Sarah', 'Lim', NULL, 1, '["Mathematics"]', NULL, 20),
	(87, NULL, 'Karen_Lorenz', 'Karen', 'Lorenz', NULL, 1, '["Mathematics"]', NULL, 20),
	(88, NULL, 'Greg_Metzger', 'Greg', 'Metzger', NULL, 1, '["Mathematics"]', NULL, 20),
	(89, NULL, 'Lindsey_Metzler', 'Lindsey', 'Metzler', NULL, 1, '["Mathematics", "Special Education"]', NULL, 20),
	(90, NULL, 'Kate_O\'Brien', 'Kate', 'O\'Brien', NULL, 1, '["Mathematics"]', NULL, 20),
	(91, NULL, 'Allyson_O\'Neill', 'Allyson', 'O\'Neill', NULL, 1, '["Mathematics"]', NULL, 20),
	(92, NULL, 'Paula_Roberts', 'Paula', 'Roberts', NULL, 1, '["Mathematics"]', NULL, 20),
	(93, NULL, 'Christopher_Campbell', 'Christopher', 'Campbell', NULL, 1, '["Music"]', NULL, 20),
	(94, NULL, 'Tobias_Morris', 'Tobias', 'Morris', NULL, 1, '["Music"]', NULL, 20),
	(95, NULL, 'Laurel_Wacyk', 'Laurel', 'Wacyk', NULL, 1, '["Music"]', NULL, 20),
	(96, NULL, 'William_Zuccarini', 'William', 'Zuccarini', NULL, 1, '["Music"]', NULL, 20),
	(97, NULL, 'James_Creighton', 'James', 'Creighton', NULL, 1, '["PE/Health"]', NULL, 20),
	(99, NULL, 'ross_hannon', 'Ross', 'Hannon', NULL, 1, '["PE/Health"]', NULL, 20),
	(100, NULL, 'Joshua_McGoun', 'Joshua', 'McGoun', NULL, 1, '["PE/Health"]', NULL, 20),
	(101, NULL, 'Alexandra_Taylor', 'Alexandra', 'Taylor', NULL, 1, '["PE/Health"]', NULL, 20),
	(102, NULL, 'Seth_Willingham', 'Seth', 'Willingham', NULL, 1, '["PE/Health"]', NULL, 20),
	(103, NULL, 'Grant_Scott', 'Grant', 'Scott', NULL, 1, '["Science"]', NULL, 20),
	(104, NULL, 'Rita_Allan', 'Rita', 'Allan', NULL, 1, '["Science"]', NULL, 20),
	(105, NULL, 'Catherine_Bloedorn', 'Catherine', 'Bloedorn', NULL, 1, '["Science"]', NULL, 20),
	(106, NULL, 'Kate_Castellucci', 'Kate', 'Castellucci', NULL, 1, '["Science"]', NULL, 20),
	(107, NULL, 'David_Klotz', 'David', 'Klotz', NULL, 1, '["Science"]', NULL, 20),
	(108, NULL, 'Brandon_Kovalski', 'Brandon', 'Kovalski', NULL, 1, '["Science"]', NULL, 20),
	(109, NULL, 'Stephanie_Mabrey', 'Stephanie', 'Mabrey', NULL, 1, '["Science"]', NULL, 20),
	(110, NULL, 'Paul_Marcantonio', 'Paul', 'Marcantonio', NULL, 1, '["Science"]', NULL, 20),
	(111, NULL, 'Jodie_Molchany', 'Jodie', 'Molchany', NULL, 1, '["Science"]', NULL, 20),
	(112, NULL, 'allison_ose', 'Allison', 'Ose', NULL, 1, '["Science"]', NULL, 20),
	(113, NULL, 'Michelle_Rugiel', 'Michelle', 'Rugiel', NULL, 1, '["Science"]', NULL, 20),
	(114, NULL, 'Jim_Seale', 'Jim', 'Seale', NULL, 1, '["Science"]', NULL, 20),
	(115, NULL, 'Tonia_Thomas-Padilla', 'Tonia', 'Thomas-Padilla', NULL, 1, '["Science"]', NULL, 20),
	(116, NULL, 'Karly_Trinite', 'Karly', 'Trinte', NULL, 1, '["Science"]', NULL, 20),
	(117, NULL, 'Erika_Wunderlich-Hite', 'Erika', 'Wunderlich-Hite', NULL, 1, '["Science"]', NULL, 20),
	(118, NULL, 'Jon_Hollander', 'Jon', 'Hollander', NULL, 1, '["Social Studies"]', NULL, 20),
	(119, NULL, 'Brian_Boussy', 'Brian', 'Boussy', NULL, 1, '["Social Studies"]', NULL, 20),
	(120, NULL, 'Danelle_Brennan', 'Danelle', 'Brennan', NULL, 1, '["Social Studies"]', NULL, 20),
	(121, NULL, 'christian_callender', 'Christian', 'Callender', NULL, 1, '["Social Studies"]', NULL, 20),
	(122, NULL, 'shea_conway', 'Shea', 'Conway', NULL, 1, '["Social Studies", "Special Education"]', NULL, 20),
	(133, NULL, 'christina_costabile', 'Christina', 'Costabile', NULL, 1, '["Social Studies"]', NULL, 20),
	(135, NULL, 'leah_foran', 'Leah', 'Foran', NULL, 1, '["Social Studies"]', NULL, 20),
	(136, NULL, 'Jennifer_Ford', 'Jennifer', 'Ford', NULL, 1, '["Social Studies"]', NULL, 20),
	(137, NULL, 'brian_gertzen', 'Brian', 'Gertzen', NULL, 1, '["Social Studies"]', NULL, 20),
	(138, NULL, 'SharonN_Goldsmith', 'Sharon', 'Goldsmith', NULL, 1, '["Social Studies"]', NULL, 20),
	(139, NULL, 'Douglas_Kaplan', 'Douglas', 'Kaplan', NULL, 1, '["Social Studies"]', NULL, 20),
	(140, NULL, 'Carolyn_Mager', 'Carolyn', 'Mager', NULL, 1, '["Social Studies"]', NULL, 20),
	(141, NULL, 'Masami_Stratton', 'Masami', 'Stratton', NULL, 1, '["Social Studies"]', NULL, 20),
	(142, NULL, 'David_Sleichter', 'David', 'Sleichter', NULL, 1, '["Social Studies"]', NULL, 20),
	(143, NULL, 'Kristyn_Walger-Magday', 'Kristyn', 'Walger-Magday', NULL, 1, '["Social Studies"]', NULL, 20),
	(144, NULL, 'John_Webster', 'John', 'Webster', NULL, 1, '["Social Studies"]', NULL, 20),
	(145, NULL, 'Tracey_Richards', 'Tracey', 'Richards', NULL, 1, '["Special Education"]', NULL, 20),
	(146, NULL, 'michelle_bowers', 'Michelle', 'Bowers', NULL, 1, '["Special Education"]', NULL, 20),
	(147, NULL, 'heather_clausen', 'Heather', 'Clausen', NULL, 1, '["Special Education"]', NULL, 20),
	(149, NULL, 'Zachary_Dickerson', 'Zachary', 'Dickerson', NULL, 1, '["Special Education"]', NULL, 20),
	(150, NULL, 'sarah_hammond', 'Sarah', 'Hammond', NULL, 1, '["Special Education"]', NULL, 20),
	(152, NULL, 'Daneace_Jeffery', 'Daneace', 'Jeffery', NULL, 1, '["Special Education"]', NULL, 20),
	(154, NULL, 'Michael_Kellaher', 'Michael', 'Kellaher', NULL, 1, '["Special Education"]', NULL, 20),
	(155, NULL, 'Jessica_McCay', 'Jessica', 'McCay', NULL, 1, '["Special Education"]', NULL, 20),
	(157, NULL, 'Brian_Murphy', 'Brian', 'Murphy', NULL, 1, '["Special Education"]', NULL, 20),
	(159, NULL, 'Veronica_Renna', 'Veronica', 'Renna', NULL, 1, '["Special Education"]', NULL, 20),
	(160, NULL, 'erica_santiago', 'Erica', 'Santiago', NULL, 1, '["Special Education"]', NULL, 20),
	(161, NULL, 'barbara_lokitis', 'Barbara', 'Lokitis', NULL, 1, '["Special Education"]', NULL, 20),
	(162, NULL, 'Sharon_Sowada', 'Sharon', 'Sowada', NULL, 1, '["Special Education"]', NULL, 20),
	(163, NULL, 'natalie_stiles', 'Natalie', 'Stiles', NULL, 1, '["Special Education"]', NULL, 20),
	(165, NULL, 'jessica_young', 'Jessica', 'Young', NULL, 1, '["Special Education"]', NULL, 20),
	(166, NULL, 'Shante_Young', 'Shante', 'Young', NULL, 1, '["Special Education"]', NULL, 20),
	(167, NULL, 'Patty_Allis-May', 'Patty', 'Allis-May', NULL, 1, '["Special Education"]', NULL, 20),
	(168, NULL, 'Kathryn_Davis', 'Kathryn', 'Davis', NULL, 1, '["Special Education"]', NULL, 20),
	(169, NULL, 'Larisa_Ermakova', 'Larisa', 'Ermakova', NULL, 1, '["Special Education"]', NULL, 20),
	(170, NULL, 'shipra_garg', 'Shipra', 'Garg', NULL, 1, '["Special Education"]', NULL, 20),
	(171, NULL, 'Anne_Handy', 'Anne', 'Handy', NULL, 1, '["Special Education"]', NULL, 20),
	(172, NULL, 'Donna_Hayman', 'Donna', 'Hayman', NULL, 1, '["Special Education"]', NULL, 20),
	(173, NULL, 'Willie_Spence', 'Willie', 'Spence', NULL, 1, '["Special Education"]', NULL, 20),
	(174, NULL, 'Barbara_Ek', 'Barbara', 'Ek', NULL, 1, '["Special Education"]', NULL, 20),
	(175, NULL, 'Elizabeth_Erikson', 'Elizabeth', 'Erikson', NULL, 1, '["Special Education"]', NULL, 20),
	(177, NULL, 'Juliet_Casanova', 'Jules', 'Casanova', NULL, 1, '["Technology Ed"]', NULL, 20),
	(179, NULL, 'Julius_Morraye', 'Julius', 'Morraye', NULL, 1, '["Technology Ed"]', NULL, 20),
	(181, NULL, 'laura_greffen', 'Laura', 'Greffen', NULL, 1, '["Theatre"]', NULL, 20),
	(182, NULL, 'Julia_Greiwe-Martinez', 'Julia', 'Greiwe-Martinez', NULL, 1, '["World Language/ESOL"]', NULL, 20),
	(183, NULL, 'John_Arnold', 'John', 'Arnold', NULL, 1, '["World Language/ESOL"]', NULL, 20),
	(184, NULL, 'Jennifer_Borgerding', 'Jennifer', 'Borgerding', NULL, 1, '["World Language/ESOL"]', NULL, 20),
	(185, NULL, 'Giuliana_Conti', 'Giuliana', 'Conti', NULL, 1, '["World Language/ESOL"]', NULL, 20),
	(186, NULL, 'James_Fetterman', 'James', 'Fetterman', NULL, 1, '["World Language/ESOL"]', NULL, 20),
	(187, NULL, 'Stephanie_Ghezzi', 'Stephanie', 'Ghezzi', NULL, 1, '["World Language/ESOL"]', NULL, 20),
	(188, NULL, 'Victoria_Picciano', 'Victoria', 'Picciano', NULL, 1, '["World Language/ESOL"]', NULL, 20),
	(189, NULL, 'laura_holland', 'Laura', 'Holland', NULL, 1, '["World Language/ESOL"]', NULL, 20),
	(190, NULL, 'Alexandra_javier', 'Alexandra', 'Javier', NULL, 1, '["World Language/ESOL"]', NULL, 20),
	(191, NULL, 'Megan_Lazzor', 'Megan', 'Lazzor', NULL, 1, '["World Language/ESOL"]', NULL, 20),
	(192, NULL, 'Tyler_Petrini', 'Tyler', 'Petrini', NULL, 1, '["World Language/ESOL"]', NULL, 20),
	(193, NULL, 'Amy_Rill', 'Amy', 'Rill', NULL, 1, '["World Language/ESOL"]', NULL, 20),
	(194, NULL, 'Emily_Vargo', 'Emily', 'Vargo', NULL, 1, '["World Language/ESOL"]', NULL, 20),
	(195, NULL, 'GPalermo', 'Gina', 'Palermo', NULL, 1, '["Athletics"]', NULL, 20);
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;


-- lionstime 데이터베이스 구조 내보내기
DROP DATABASE IF EXISTS `lionstime`;
CREATE DATABASE IF NOT EXISTS `lionstime` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `lionstime`;

-- 테이블 lionstime.params 구조 내보내기
DROP TABLE IF EXISTS `params`;
CREATE TABLE IF NOT EXISTS `params` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(50) NOT NULL,
  `value` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 테이블 데이터 lionstime.params:~2 rows (대략적) 내보내기
DELETE FROM `params`;
/*!40000 ALTER TABLE `params` DISABLE KEYS */;
INSERT INTO `params` (`id`, `key`, `value`) VALUES
	(1, 'enabled', 'true'),
	(2, 'week', '0');
/*!40000 ALTER TABLE `params` ENABLE KEYS */;

-- 테이블 lionstime.passes 구조 내보내기
DROP TABLE IF EXISTS `passes`;
CREATE TABLE IF NOT EXISTS `passes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `teacher` text NOT NULL,
  `purpose` text NOT NULL,
  `time` text NOT NULL,
  `week` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 lionstime.passes:~0 rows (대략적) 내보내기
DELETE FROM `passes`;
/*!40000 ALTER TABLE `passes` DISABLE KEYS */;
/*!40000 ALTER TABLE `passes` ENABLE KEYS */;

-- 테이블 lionstime.settings 구조 내보내기
DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(50) NOT NULL,
  `value` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- 테이블 데이터 lionstime.settings:~7 rows (대략적) 내보내기
DELETE FROM `settings`;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `key`, `value`, `description`) VALUES
	(1, 'school', 'Howard', 'School Name'),
	(2, 'name', 'Lion\'s Time', 'Name of the activity'),
	(3, 'day', '3', 'Weekday the activity is on - 0 is Sunday, 6 is Saturday'),
	(6, 'schoolEndHrs', '14', 'School end time\'s hour - 0 to 23'),
	(7, 'schoolEndMins', '10', 'School end time\'s minute = 0 to 59'),
	(8, 'purposes', '["Homework", "Classwork", "Take a quiz", "Other"]', 'List of purposes In JSON format. INCORRECT FORMATTING MAY BREAK THE PROGRAM!'),
	(9, 'depts', '["Administration", "Student Services", "Art", "Athletics", "BCMS", "BSAP", "CRD", "Dance", "English", "FACS", "G/T", "JROTC", "Media Center", "Mathematics", "Music", "PE/Health", "Science", "Social Studies", "Special Education" ,"Teacher Academy", "Technology Ed", "Theatre", "World Language/ESOL"]', 'List of departments In JSON Format. INCORRECT FORMATTING MAY BREAK THE PROGRAM!');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

